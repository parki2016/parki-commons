package au.com.parki.sns.model;

public class SNSNotification {

    private String body;
    private String title;
    private String deviceToken;
    private String customData;

    public SNSNotification() {}
    public SNSNotification(String body, String title, String deviceToken, String customData) {
        this.body = body;
        this.title = title;
        this.deviceToken = deviceToken;
        this.customData = customData;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getCustomData() {
        return customData;
    }

    public void setCustomData(String customData) {
        this.customData = customData;
    }

    public boolean isIOS() {
        return deviceToken.length() == 64;
    }
}
