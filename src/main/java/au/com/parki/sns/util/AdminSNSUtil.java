package au.com.parki.sns.util;

import au.com.parki.sns.model.SNSCred;
import au.com.parki.sns.model.SNSNotification;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.*;
import com.amazonaws.util.StringUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class AdminSNSUtil implements Runnable {

    public AdminSNSUtil(String message, String deviceToken){
        this.amazonSNSClient = (AmazonSNSClient) AmazonSNSClientBuilder
                .standard()
                .withRegion(Regions.AP_SOUTHEAST_2)
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(SNSCred.ACCESS, SNSCred.SECRET)))
                .build();
        this.message = message;
        this.deviceToken = deviceToken;
    }
    private final AmazonSNSClient amazonSNSClient;
    private final String message;
    private final String deviceToken;

    private static final String IOS_ARN = "arn:aws:sns:ap-southeast-2:857899259601:app/APNS/ParkiAdminIOS";
    private static final String ANDROID_ARN = "arn:aws:sns:ap-southeast-2:857899259601:app/GCM/ParkiAdminAndroid";

    private static final String MOBILE_NOTIFICATION_TITLE = "PARKi Admin";
    private static final String MOBILE_NOTIFICATION_CUSTOM_DATA = "PARKi Admin Notification";
    @Override
    public void run() {
        this.sendNotification();
    }

    private void sendNotification(){
        if(StringUtils.isNullOrEmpty(deviceToken)) return;
        SNSNotification snsNotification = new SNSNotification(message, MOBILE_NOTIFICATION_TITLE, deviceToken, MOBILE_NOTIFICATION_CUSTOM_DATA);
        PublishRequest request = new PublishRequest();
        request.setMessageStructure("json");
        request.setTargetArn(createDeviceEndpoint(snsNotification));
        try {
            request.setMessage( snsNotification.isIOS() ? getIOSMsg(snsNotification.getBody()) : getAndroidMsg(snsNotification.getBody()));
            amazonSNSClient.publish(request);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (EndpointDisabledException e) {
            System.out.println("SNS : error pushing notification : "+deviceToken);
        }
    }

    private String createDeviceEndpoint(SNSNotification snsNotification) throws AuthorizationErrorException, InternalErrorException, InvalidParameterException, NotFoundException {
        CreatePlatformEndpointRequest request = new CreatePlatformEndpointRequest();
        request.setCustomUserData(snsNotification.getCustomData());
        request.setToken(snsNotification.getDeviceToken());
        request.setPlatformApplicationArn(snsNotification.isIOS() ? IOS_ARN : ANDROID_ARN);
        CreatePlatformEndpointResult result  = amazonSNSClient.createPlatformEndpoint(request);
        return result.getEndpointArn();
    }

    private static String getAndroidMsg(String body) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode rootNode = mapper.createObjectNode();
        ObjectNode gcm = mapper.createObjectNode();
        ObjectNode notification = mapper.createObjectNode();
        gcm.set("notification", notification);
        notification.put("body", body);
        notification.put("title", " ");
        String gcmString = mapper.writeValueAsString(gcm).replaceAll("\"", "\\\"");
        rootNode.put("GCM", gcmString);
        return mapper.writeValueAsString(rootNode);

    }

    private static String getIOSMsg(String body) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode rootNode = mapper.createObjectNode();
        ObjectNode apns = mapper.createObjectNode();
        ObjectNode aps = mapper.createObjectNode();
        apns.set("aps", aps);
        aps.put("alert", body);
        aps.put("sound", "pn_car_horn.wav");
        String apnsString = mapper.writeValueAsString(apns).replaceAll("\"", "\\\"");
        rootNode.put("APNS", apnsString);
        return mapper.writeValueAsString(rootNode);
    }
}
