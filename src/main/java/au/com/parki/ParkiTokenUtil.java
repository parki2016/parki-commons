package au.com.parki;

import au.com.parki.model.ParkiUserToken;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class ParkiTokenUtil {


    public ParkiTokenUtil() {
        String keyString = "gfMEI47CsL4mmfQG7g3EZ6rELWHfAPKQ2ac1P7Jm";
/*
        this.key = new SecretKeySpec(getSignatureKey(keyString), "HmacSHA256");
*/
        this.key = new SecretKeySpec(keyString.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
        jwtParser = Jwts.parserBuilder().setSigningKey(this.key).build();

    }

    private static byte[] HmacSHA256(String data, byte[] key) throws Exception {
        String algorithm="HmacSHA256";
        Mac mac = Mac.getInstance(algorithm);
        mac.init(new SecretKeySpec(key, algorithm));
        return mac.doFinal(data.getBytes("UTF-8"));
    }

    private static byte[] getSignatureKey(String key) throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmssZ");
        byte[] kSecret = ("AWS4" + key).getBytes("UTF-8");
        byte[] kDate = HmacSHA256(ZonedDateTime.now().format(formatter), kSecret);
        byte[] kRegion = HmacSHA256("ap-southeast-2", kDate);
        byte[] kService = HmacSHA256("apigateway", kRegion);
        byte[] kSigning = HmacSHA256("aws4_request", kService);
        return kSigning;
    }
    private static Date getExpDate(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR,14);
        return calendar.getTime();
    }
    private final SecretKeySpec key;
    private final JwtParser jwtParser;

    private static final String ISSUER = "ParkiAdmin";
    private static final String ORG = "org";
    public String getToken(ParkiUserToken parkiUserToken) {
        return Jwts.builder().setSubject(parkiUserToken.getEmail()).setIssuer(ISSUER).setExpiration(getExpDate()).claim(ORG, parkiUserToken.getOrg())
                .signWith(this.key).compact();
    }

    public ParkiUserToken getParkiUserToken(String token) {
        Claims claims = getClaims(token);
        return ParkiUserToken.getInstance(claims.getSubject(), claims.get(ORG, Long.class));
    }

    public Claims getClaims(String token) {
        return jwtParser.parseClaimsJws(token).getBody();
    }
}
