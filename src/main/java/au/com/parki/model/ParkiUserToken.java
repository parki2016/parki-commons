package au.com.parki.model;

public class ParkiUserToken {

    private final String email;
    private final Long org;

    private ParkiUserToken(String email, Long org) {
        this.email = email;
        this.org = org;
    }

    public static ParkiUserToken getInstance(String email, Long org) {
        return new ParkiUserToken(email, org);
    }

    public String getEmail() {
        return email;
    }

    public Long getOrg() {
        return org;
    }
}
